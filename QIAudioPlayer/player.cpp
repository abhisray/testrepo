#include "player.h"


#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QUrl>
#include <QDir>

#include <QCoreApplication>

#include <QDebug>

CPlayer::CPlayer()
{
    m_pMediaPlayer = new QMediaPlayer();
    m_pMediaPlaylist = new QMediaPlaylist;
}


QUrl
CPlayer::GetMediaUrl(QString &fileName)
{
    QUrl soundUrl;
    if(QDir::isAbsolutePath(fileName))
    {
        soundUrl = QUrl::fromLocalFile(fileName);
    }
    else
    {
        QDir curdir(QCoreApplication::applicationDirPath());
        QDir absolute_file_dir(curdir.absoluteFilePath(fileName));
        QString strAbsFilePath = absolute_file_dir.absolutePath();
        soundUrl = QUrl::fromLocalFile(strAbsFilePath);
    }
    return soundUrl;
}


void
CPlayer::Play(QString& strAudioFilePath)
{
    if(m_pMediaPlayer)
    {
        QUrl soundURL = GetMediaUrl(strAudioFilePath);
        m_pMediaPlayer->setMedia(soundURL);
        m_pMediaPlayer->setVolume(100);
        m_pMediaPlayer->play();
    }
}


void
CPlayer::Play(QStringList& audioFilePathList)
{
    for (int i = 0; i < audioFilePathList.size(); ++i)
    {
        QString audioFilePath = audioFilePathList.at(i);
        qDebug() << "audio file playing : " << audioFilePath;
        AddAudioToPlaylist(audioFilePath);
    }
    Play();
}


void
CPlayer::AddAudioToPlaylist(QString& strAudioFile)
{
    if(m_pMediaPlaylist)
    {
        QUrl soundURL = GetMediaUrl(strAudioFile);
        m_pMediaPlaylist->addMedia(soundURL);
    }
}


void
CPlayer::Play()
{
    m_pMediaPlayer->setPlaylist(m_pMediaPlaylist);
    m_pMediaPlayer->play();
}
