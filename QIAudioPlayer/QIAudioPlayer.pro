#-------------------------------------------------
#
# Project created by QtCreator 2017-11-29T23:17:09
#
#-------------------------------------------------

QT       += core multimedia

QT       -= gui

TARGET = QIAudioPlayer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    player.cpp

HEADERS += \
    player.h
