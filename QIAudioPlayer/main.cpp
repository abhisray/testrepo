


#include "player.h"

#include <QCoreApplication>
#include <QStringList>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QStringList argList = a.arguments();
    //CPlayer player(argList);

    CPlayer player;
    argList.removeFirst();
    player.Play(argList);

    /*QString filePath("../audioFiles/2km.wav");
    player.AddAudioToPlaylist(filePath);

    filePath= "../audioFiles/3km.wav";
    player.AddAudioToPlaylist(filePath);
    player.Play();*/

    return a.exec();
}
