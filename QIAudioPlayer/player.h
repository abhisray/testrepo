#ifndef PLAYER_H
#define PLAYER_H

#include <QString>
//#include <QStringList>

class QMediaPlayer;
class QMediaPlaylist;
class QUrl;

class CPlayer
{
public:
    CPlayer();

    void Play(QString& strAudioFilePath);
    void Play(QStringList& audioFilePathList);

    void AddAudioToPlaylist(QString &strAudioFile);
    void AddAudioToPlaylist(QStringList& audioList);
    void Play();


private:

    QUrl GetMediaUrl(QString& fileName);

    QMediaPlayer*           m_pMediaPlayer;
    QMediaPlaylist*         m_pMediaPlaylist;
};

#endif // PLAYER_H
